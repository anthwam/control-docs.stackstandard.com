# **Health Insurance Portability & Accountability Act** 🚨

![alt text](../images/legal/HIPAA.jpg)

 ___The Health Insurance Transfer and Liability Act of 1996 was created to protect millions of workers and members of their families in the United States with medical conditions.___

# **Stackstandard in HIPAA compliance** ✔️ 


  >**Stackstandard has an encryption process which allows the confidentiality of the data that is processed.**

>**Generating guarantees of confidentiality, integrity and availability of all e-PHI that they create, receive, maintain or transmit.**

>**Identifying and protecting against reasonably anticipated threats to the security or integrity of the information.**

>**Protection Against Reasonably Anticipated and Unacceptable Uses or Disclosures.**
