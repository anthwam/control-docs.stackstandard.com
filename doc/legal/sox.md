# **SOX** 📜

![alt text](../images/legal/sox.jpeg)

## ___Regulation law on financial accounting and auditing functions___

>*SOX controls the process of maintaining records of accounts and transactions for large public and private companies, requiring that the data be kept for at least 5 years.*

>*Currently, the companies that must comply with this Law are those public companies that are registered with the SEC - Security and Exchange Commission, which is the entity that regulates companies listed on the United States stock exchange, that is, that they are listed on this exchange; This includes its affiliates.*

# **Stackstandard in SOX compliance** ✔️

**Because our business model from Stackstandar is to offer a standard of usability, rigidity, security and support to our clients in the development of their products and by belonging to the GLOBAL DEVELOPER NETWORK (GDN) collective, we take on the task of complying with the regulation main SOX.**