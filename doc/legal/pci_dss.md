# **Payment Card Industry Data Security Standard – PCI DSS** 💳 

![alt text](../images/legal/pcidss.png)

***The Payment Card Industry Data Security Standard (PCI DSS) is a security standard published by the PCI SSC and aimed at defining controls for the protection of the holder's data. the card and / or confidential authentication data during its processing, storage and / or transmission. Currently, it is in version 4.0.0 published on August 05, 2019.***

# **Stackstandard in PCI-DSS compliance** ✔️

***Backed by the certification we have on PCI, we guarantee total transparency and security methods for our clients, followed by the support of PCI guarantors of the commitment in the certification of good security methodologies for the client***

![alt text](../images/legal/pci_c.png)
