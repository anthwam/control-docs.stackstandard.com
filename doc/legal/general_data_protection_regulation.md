# **General Data Protection Regulation** 🔒

![alt text](../images/legal/gdpr-compliance.png)

### ___The General Data Protection Regulation is the European regulation on the protection of natural persons with regard to the processing of their personal data and the free circulation of these data.___   

## ___So who does the GDPR apply to?___
*Data "controllers" and "processors" must abide by the GDPR. A data controller indicates how and why personal data is processed, while a processor is the party that performs the actual processing of the data. Therefore, the controller could be any organization, from a for-profit company to a charity or a government. A processor could be an IT company that does the actual data processing.*

*Even if the controllers and processors are outside the EU, the GDPR will still apply to them as long as it concerns data belonging to EU residents.*

*It is the controller's responsibility to ensure that its processor complies with data protection law and processors must adhere to the rules for keeping records of their processing activities. If processors are involved in a data breach, they are much more liable under GDPR than they were under the Data Protection Act.*

## ___What is personal data under the GDPR?___

*The EU has substantially expanded the definition of personal data under the GDPR. To reflect the types of data organizations that now collect about individuals, online identifiers, such as IP addresses, are now considered personal data. Other data, such as economic, cultural or mental health information, is also considered personally identifiable information.*

*Pseudonymous personal data may also be subject to GDPR rules, depending on how easy or difficult it is to identify what the data is.*

*Anything that was considered personal data under the Data Protection Act also qualifies as personal data under the GDPR.*

# **Stackstandard in GDPR compliance** ✔️

___At Stack Standard, our main priority in data processing is to guarantee our clients the correct treatment of the same, including a very secure encryption and tokenization standard so that the data is fully protected throughout its life cycle from its collection to its possible destruction.___

