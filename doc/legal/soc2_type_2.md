# **SOC 2 TYPE 2** 📜

![alt text](../images/legal/soc2.png)

## ___What is SOC 2 Type 2?___

*With SOC 2 Type 2 report, a service firm can send a powerful message to potential customers that it applies the best practices on data security and control systems.*

# **Stackstandard in SOC TYPE 2 compliance** ✔️

**Despite being a completely voluntary compliance by companies with the SOC TYPE 1 certificate, at Stackstandar we want to guarantee our partners and clients the highest possible compliance in terms of security and agile processes, which allow us to have an effective and efficient audience .**