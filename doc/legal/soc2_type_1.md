# **SOC 2 TYPE 1** 📜

![alt text](../images/legal/soc2.png)

## ___What is SOC 2 Type 1?___

*To be more specific, a SOC 2 Type 1 report details the suitability of the design controls to the service organization’s system. It details the system at a point in time particularly its scope, the management of the organization describing the system, and the controls in place.*

*Key to this report is its ‘as of’ date meaning it deals with the specifics of a system within a particular point in time.  The auditor will base his or her report on the description of the controls and review of documentation around these controls.*

*As a proof of compliance to the AICPA auditing procedure, SOC 2 Type 1 report shows that a SaaS firm has best practices in place. There are numerous benefits that this report can provide to any service entity.*

*SOC 2 Type 1 report is particularly helpful to service companies as it can make them more competitive. It gives potential customers the assurance that a service organization has passed the said auditing procedure, and that their data is safe if they work with the SOC 2-compliant company.*

# **Stackstandard in SOC TYPE 1 compliance** ✔️

**Despite being a completely voluntary compliance by companies with the SOC TYPE 1 certificate, at Stackstandar we want to guarantee our partners and clients the highest possible compliance in terms of security and agile processes, which allow us to have an effective and efficient audience .**