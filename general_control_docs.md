# **Stackstandard (GDN) Security Compliance Controls** 🚀

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)


## This is the source for the [Stackstandar](https://stackstandard.com/) site. For a guide on how to start editing the website using stackstandard, see the handbook page on that topic.

**Stackstandard an a security Index, and a registry of trusted packages that correspond to signatures in Git and relayed through NPM if you pull code with the Github Packages API.**

Security controls are a way to state our company's position on a variety of security topics. It's not enough to simply say "We encrypt data" since our customers and teams will naturally want to know "what data do we encrypt?" and "how do we encrypt that data?". When all of our established security controls are operating effectively this creates a security program greater than the sum of its parts that will demonstrate to our stakeholders that Stackstandard has a mature and comprehensive security program that will provide assurance that data within Stackstandard is reasonably protected.

## Impulsed by Global Developer Network (GDN) 

**Open source provider of Full Stack Engine specifications utilizing the open Stackstandard specification.** 
**(stackstandard.com). Mandate is to drive adoption in various frontend, middleware, and backend OSS communities.**

# **Stackstandard Control Framework (GDN)** 📋

Given our efficiency value here at LTD, we wanted to create a set of security controls that would address multiple underlying requirements with a single security control that would allow us to make fewer requests to our internal teams and efficiently collect all the evidence we would need for a variety of audits at once. This approach can be visualized as follows:

![alt text](./images/EKS_Chart_Helm__AWS.png)

As our security compliance objectives and requirements have evolved, so have our requirements and constraints related to our security control framework.

# **Industry Standard Compliance** ⚙️

## **Technical Specifications** 💻

*  Advisory Council of Security Experts
        
* Stackstandard.com is actively assembling a security review board made up of experts from a handful of elite roles in the tech industry. This council provides 3rd party oversight and advice on how we can best meet the security needs of our consumers.
* International Review Board - For Research Studies (behavioral studies)
* Peer reviewed specifications and testing
* SOC II Type 2 Certified
* EU-US Privacy Shield Framework
* GDPR
* Data Encryption at rest and in transit
* Support for TLS Protocol v 1.2 and v 1.3
* HIPAA Compliant option
  * HIPAA Rules: Comply with all aspects of the rules that make up HIPAA; the Privacy Rule, Security Rule, HITECH and the Omnibus Rule.
  * Security Safeguards: The administrative, physical and technical safeguards laid out in the Security Rule should be followed.
  * Transport Encryption: Any electronic health information (ePHI) must be encrypted before it is shared or disclosed
  * Backup: All ePHI should be backed up in case there is a need to recover or restore the information.
  * Authorization: ePHI should be restricted so that it is only accessible to authorized personnel.
  * Storage Encryption: In addition to how it is shared, ePHI should also be stored in an encrypted manner.
  * Integrity: ePHI should not be available to unauthorized changes or improper destruction.
  * Disposal: Once the ePHI is not needed anymore, it should be safely and permanently destroyed.
  * Business Associate Agreement: As we have mentioned above, software companies that hold or share PHI must sign business associate agreements with the covered entities that they will be vendors for. Once completed, these agreements should be held on secure servers.

# **Security tools** 🔒


* [Notary](https://github.com/theupdateframework/notary)

* [Sentry](https://sentry.io/welcome/)

* [Airbrake](https://airbrake.io/)

* [Guardrailsio](https://github.com/guardrailsio)

* [Marketplace - security](https://github.com/marketplace/category/security)
  

# **GDPR checklist** 	✔️

* Obtain Cyber Essentials Security certification.
* Thoroughly research the areas of our product and business impacted by GDPR.
* Appoint a Data Protection Officer.
* Develop a strategy and guidelines for how to address the areas of our product impacted by GDPR.
* Perform necessary changes to the platform.
* Evolve our internal processes and procedures.
* Thoroughly test all changes under the GDPR.
* Update our privacy policy.
* Rewrite our Data Processing Agreement.
* Communicate our compliance.
* Obtain independent GDPR compliance certification.
* Microsoft certifications for Data Security in the Dynamics system.

# **Foresite CCCS Team Certifications** 📝

* [CCISO](https://ciso.eccouncil.org/cciso-certification/)
* [CISSP](https://www.isc2.org/Certifications/CISSP)
* [MCITP Enterprise Administrator](https://docs.microsoft.com/es-es/learn/certifications/browse/?resource_type=certification)
* [PCI](https://www.pcisecuritystandards.org/)
* [PCIP PCI Professional](https://www.pcisecuritystandards.org/program_training_and_qualification/pci_professional_qualification)
* [GSNA (Security / Network Auditor)](http://www.giac.org/certification/systems-network-auditor-gsna)
* [GCIH (Certified Incident Handler)](http://www.giac.org/certification/certified-incident-handler-gcih)
* [GPEN (Penetration Testing)](http://www.giac.org/certification/penetration-tester-gpen)
* [CHPSE Certified HIPAA Privacy & Security Expert](https://www.hipaatraining.net/certified-privacy-security-expert/)
* [HCISPP Healthcare Certified Information Security and Privacy Practitioner](https://www.isc2.org/hcispp/default.aspx)
* [JNCIP-SEC Juniper Networks Certified Internet Professional Security](http://www.juniper.net/us/en/training/certification/certification-tracks/junos-security-track/)
* [JNCIP-ENT Juniper Networks Certified Internet Professional Enterprise Switching and Routing](http://www.juniper.net/us/en/training/certification/certification-tracks/ent-routing-switching-track/)
* [JNCSP-ENT Juniper Networks Certified Support Professional Enterprise Switching and Routing](http://www.juniper.net/us/en/training/certification/certification-tracks/ent-routing-switching-track/)
* [Check Point](https://www.checkpoint.com/support-services/training-certification/checkpoint-certified-security-administrator-ccsa/)
* [CEHv8 Certified Ethical Hacker](https://www.eccouncil.org/programs/certified-ethical-hacker-ceh/)
* [Comptia – Security+](https://certification.comptia.org/certifications/security)
* [Comptia – CySA+]()
* [Comptia – Pentest+]()
* [Comptia – A+]()
* [Microsoft Azure Security Engineer]()
* [MCSE Microsoft Certified Systems Engineer Server 2012](https://www.microsoft.com/en-us/learning/mcse-certification.aspx)
* [MCITP Microsoft Certified IT Professional – Exchange 2010](https://www.microsoft.com/en-us/learning/mcitp-certification.aspx)
* [Cisco CCENR, CCNA]()
* [Check Point CCSA]()
* [Palo Alto PCNSE]()
* [Juniper JNCIP-SEC](http://www.juniper.net/us/en/training/certification/certification-tracks/junos-security-track/)
* [Juniper JNCIS-ENT](http://www.juniper.net/us/en/training/certification/certification-tracks/ent-routing-switching-track/)
* Microsoft Azure Security Engineer
* [Microsoft Certified Professional (MCP)](https://www.microsoft.com/en-us/learning/microsoft-certified-professional.aspx)
* OSCP
* [ISO 27001: 2013](http://www.iso.org/iso/home/standards/management-standards/iso27001.htm)
* PCI MSSP Service Provider

# **IP Address Blocking** 🔐  

***Check whether a given IP address is “Internet background noise”, or has been observed scanning or attacking devices across the Internet.***

**Notes**

- This API endpoint is real-time
- This API endpoint contains a “code” which correlates to why GreyNoise labeled the IP as "noise"
- An IP delivered via this endpoint does not include a “malicious” or “benign” categorizations
- This API endpoint only checks against the last 60 days of Internet scanner data

**Code**

- 0x00: The IP has never been observed scanning the Internet
- 0x01: The IP has been observed by the GrayNoise sensor network
- 0x02: The IP has been observed scanning the GreyNoise sensor network, but has not completed a full connection, meaning this can be spoofed
- 0x03: The IP is adjacent to another host that has been directly observed by the GreyNoise sensor network
- 0x04: Reserved
- 0x05: This IP is commonly spoofed in Internet-scan activity
- 0x06: This IP has been observed as noise, but this host belongs to a cloud provider where IPs can be cycled frequently
- 0x07: This IP is invalid
- 0x08: This IP was classified as noise, but has not been observed engaging in Internet-wide scans or attacks in over 60 days